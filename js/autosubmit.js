(function($) {
  'use strict';

  Drupal.behaviors.autosubmit = {
    attach: function(context) {
      $(':submit[data-drupal-autosubmit]', context).parents('form').submit();
    }
  };
})(jQuery);
