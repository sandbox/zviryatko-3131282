<?php


namespace Drupal\openid_connect_azure\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openid_connect\Form\LoginForm as LoginFormBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class LoginForm
 *
 * @package Drupal\openid_connect_azure\Form
 */
class LoginForm extends LoginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $client_name = NULL) {
    $definitions = $this->pluginManager->getDefinitions();
    $path = \Drupal::request()->getPathInfo();
    foreach (array_keys($definitions) as $client_id) {
      $config = $this->config('openid_connect.settings.' . $client_id);
      if (!$config->get('enabled')) {
        continue;
      }
      $configuration = $config->get('settings');
      if ($configuration['redirect_url'] === $path) {
        $client_name = $client_id;
        break;
      }
    }
    if (empty($definitions[$client_name])) {
      throw new NotFoundHttpException();
    }
    $client = $definitions[$client_name];

    $message = $this->t('You are getting logged in. Please wait…');
    if (method_exists('\Drupal', 'messenger')) {
      \Drupal::messenger()->addStatus($message);
    }
    elseif (function_exists('drupal_set_message')) {
      drupal_set_message($message);
    }

    $form['openid_connect_client_' . $client_name . '_login'] = [
      '#type' => 'submit',
      '#value' => t('Log in with @client_title', [
        '@client_title' => $client['label'],
      ]),
      '#name' => $client_name,
      '#attributes' => [
        'class' => ['visually-hidden', 'js-hide'],
        'data-drupal-autosubmit' => 'true',
      ],
      '#attached' => ['library' => ['openid_connect_azure/autosubmit']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Fix infinite redirect loop since we allowing to use custom login path.
    $current_path = \Drupal::service('path.current')->getPath();
    $path = strpos($current_path, '/user/login') !== FALSE ? '/user' : $current_path;

    // The destination could contain query parameters. Ensure that they are
    // preserved.
    $query = \Drupal::request()->getQueryString();

    $_SESSION['openid_connect_destination'] = [
      $path,
      ['query' => $query],
    ];
  }
}
