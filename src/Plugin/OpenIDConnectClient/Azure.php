<?php

namespace Drupal\openid_connect_azure\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Drupal\Core\Url;
use Drupal\openid_connect\StateToken;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom OpenID Connect client.
 *
 * @OpenIDConnectClient(
 *   id = "azure",
 *   label = @Translation("Azure")
 * )
 */
class Azure extends OpenIDConnectClientBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['label'] = [
      '#title' => $this->t('Button label'),
      '#type' => 'textfield',
      '#default_value' => !empty($this->configuration['label']) ? $this->configuration['label'] : '',
      '#weight' => -1,
    ];
    $form['redirect_url'] = [
      '#title' => $this->t('Redirect URL'),
      '#type' => 'textfield',
      '#default_value' => !empty($this->configuration['redirect_url']) ? $this->configuration['redirect_url'] : '',
      '#suffix' => $this->t('Relative url or empty for default.'),
      '#element_validate' => [[get_class($this), 'validateFormElement']],
    ];
    $form['authorization_endpoint'] = [
      '#title' => $this->t('Authorization endpoint'),
      '#type' => 'textfield',
      '#default_value' => !empty($this->configuration['authorization_endpoint']) ? $this->configuration['authorization_endpoint'] : '',
    ];
    $form['token_endpoint'] = [
      '#title' => $this->t('Token endpoint'),
      '#type' => 'textfield',
      '#default_value' => !empty($this->configuration['token_endpoint']) ? $this->configuration['token_endpoint'] : '',
    ];
    $form['userinfo_endpoint'] = [
      '#title' => $this->t('UserInfo endpoint'),
      '#type' => 'textfield',
      '#default_value' => !empty($this->configuration['userinfo_endpoint']) ? $this->configuration['userinfo_endpoint'] : '',
    ];

    return $form;
  }

  /**
   * Form element validation handler for URL alias form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateFormElement(array &$element, FormStateInterface $form_state) {
    $redirect_url = rtrim(trim($element['#value']), " \\/");
    if ($redirect_url) {
      if ($redirect_url[0] !== '/') {
        $form_state->setError($element, t('The url needs to start with a slash.'));
      }
      else {
        /** @var \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder */
        $routeBuilder = \Drupal::service('router.builder');
        $routeBuilder->rebuild();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints() {
    return [
      'authorization' => $this->configuration['authorization_endpoint'],
      'token' => $this->configuration['token_endpoint'],
      'userinfo' => $this->configuration['userinfo_endpoint'],
    ];
  }

  /**
   * Implements OpenIDConnectClientInterface::retrieveIDToken().
   *
   * @param string $authorization_code
   *   A authorization code string.
   *
   * @return array|bool
   *   A result array or false.
   */
  public function retrieveTokens($authorization_code) {
    // Exchange `code` for access token and ID token.
    $endpoints = $this->getEndpoints();

    $request_options = [
      'form_params' => [
        'code' => $authorization_code,
        'client_id' => $this->configuration['client_id'],
        'client_secret' => $this->configuration['client_secret'],
        'redirect_uri' => $this->getRedirectUrl()->getGeneratedUrl(),
        'grant_type' => 'authorization_code',
      ],
    ];

    /* @var \GuzzleHttp\ClientInterface $client */
    $client = $this->httpClient;

    try {
      $response = $client->post($endpoints['token'], $request_options);
      $response_data = json_decode((string) $response->getBody(), TRUE);

      // Expected result.
      $tokens = [
        'id_token' => $response_data['id_token'],
        'access_token' => $response_data['access_token'],
      ];
      if (array_key_exists('expires_in', $response_data)) {
        $time = method_exists('Drupal', 'time') ? \Drupal::time()->getRequestTime() : REQUEST_TIME;
        $tokens['expire'] = $time + $response_data['expires_in'];
      }

      return $tokens;
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        $message = 'Could not retrieve tokens: @response';
        $params = ['@response' => $e->getResponse()->getBody()->__toString()];
      }
      else {
        $message = 'Could not retrieve tokens: @error_message';
        $params = ['@error_message' => $e->getMessage()];
      }
      $this->loggerFactory->get('openid_connect_' . $this->pluginId)->error($message, $params);
      return FALSE;
    }
  }

  /**
   * Implements OpenIDConnectClientInterface::authorize().
   *
   * @param string $scope
   *   A string of scopes.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   A trusted redirect response object.
   */
  public function authorize($scope = 'openid email') {
    $url_options = [
      'query' => [
        'client_id' => $this->configuration['client_id'],
        'response_type' => 'code',
        'scope' => $scope,
        'redirect_uri' => $this->getRedirectUrl()->getGeneratedUrl(),
        'state' => StateToken::create(),
      ],
    ];

    $endpoints = $this->getEndpoints();
    // Clear _GET['destination'] because we need to override it.
    $this->requestStack->getCurrentRequest()->query->remove('destination');
    $authorization_endpoint = Url::fromUri($endpoints['authorization'], $url_options)->toString(TRUE);

    $response = new TrustedRedirectResponse($authorization_endpoint->getGeneratedUrl());
    // We can't cache the response, since this will prevent the state to be
    // added to the session. The kill switch will prevent the page getting
    // cached for anonymous users when page cache is active.
    \Drupal::service('page_cache_kill_switch')->trigger();

    return $response;
  }

  /**
   * Implements OpenIDConnectClientInterface::retrieveUserInfo().
   *
   * @param string $access_token
   *   An access token string.
   *
   * @return array|bool
   *   A result array or false.
   */
  public function retrieveUserInfo($access_token) {

    $endpoints = $this->getEndpoints();
    $userinfo = $this->buildUserinfo($access_token, $endpoints['userinfo'], 'upn', 'name');

    return $userinfo;
  }

  /**
   * Helper function to do the call to the endpoint and build userinfo array.
   *
   * @param string $access_token
   *   The access token.
   * @param string $url
   *   The endpoint we want to send the request to.
   * @param string $upn
   *   The name of the property that holds the Azure username.
   * @param string $name
   *   The name of the property we want to map to Drupal username.
   *
   * @return array|bool
   *   The userinfo array or FALSE.
   */
  private function buildUserinfo($access_token, $url, $upn, $name) {
    // Perform the request.
    $options = [
      'method' => 'GET',
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $access_token,
      ],
    ];
    $client = $this->httpClient;

    try {
      $response = $client->get($url, $options);
      $response_data = (string) $response->getBody();

      $this->loggerFactory->get('openid_connect_' . $this->pluginId)
        ->info('Request USER: @data', ['@data' => $response_data]);

      // Profile Information.
      $profile_data = json_decode($response_data, TRUE);
      $profile_data['name'] = $profile_data[$name] ?? '';

      if (!isset($profile_data['email'])) {
        $profile_data['email'] = $profile_data[$upn] ?? '';
      }

      return $profile_data;
    }
    catch (RequestException $e) {
      if ($e->hasResponse()) {
        $message = 'Could not retrieve user profile information. Details: @response';
        $params = ['@response' => $e->getResponse()->getBody()->__toString()];
      }
      else {
        $message = 'Could not retrieve user profile information. Details: @error_message';
        $params = ['@error_message' => $e->getMessage()];
      }
      $this->loggerFactory->get('openid_connect_' . $this->pluginId)->error($message, $params);

      return FALSE;
    }
  }

  /**
   * Get redirect URL.
   *
   * @return \Drupal\Core\GeneratedUrl
   */
  public function getRedirectUrl() {
    $language_none = \Drupal::languageManager()
      ->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE);
    $route = 'openid_connect.redirect_controller_redirect';
    if (!empty($this->configuration['redirect_url'])) {
      $route = 'openid_connect_' . $this->getPluginId() . '.redirect';
    }
    return Url::fromRoute(
      $route,
      ['client_name' => $this->pluginId],
      [
        'absolute' => TRUE,
        'language' => $language_none,
      ]
    )->toString(TRUE);
  }

}
