<?php

namespace Drupal\openid_connect_azure\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\openid_connect_azure\Routing
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * OpenID plugin manager.
   *
   * @var \Drupal\openid_connect\Plugin\OpenIDConnectClientManager
   */
  protected $pluginManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The constructor.
   *
   * @param \Drupal\openid_connect\Plugin\OpenIDConnectClientManager $plugin_manager
   *   OpenID plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(OpenIDConnectClientManager $plugin_manager, ConfigFactoryInterface $configFactory) {
    $this->pluginManager = $plugin_manager;
    $this->configFactory = $configFactory;
  }
  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $redirect = $collection->get('openid_connect.redirect_controller_redirect');
    /** @var \Drupal\openid_connect\Plugin\OpenIDConnectClientInterface[] $definitions */
    $definitions = $this->pluginManager->getDefinitions();
    foreach (array_keys($definitions) as $client_id) {
      $config = \Drupal::configFactory()->get('openid_connect.settings.' . $client_id);
      if (!$config->get('enabled')) {
        continue;
      }
      $route = clone $redirect;
      $settings = $config->get('settings');
      if (!empty($settings['redirect_url'])) {
        $route->setPath($settings['redirect_url']);
        $route->setDefault('client_name', $client_id);
        $collection->add('openid_connect_' . $client_id . '.redirect', $route);
      }
    }
  }
}
